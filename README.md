# Process Inspector

[![(c) William RM Bartlett, Creative Commons Attribution-ShareAlike 4.0 International License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

Print information about the current process.

Useful for exploring containerized processes without needing a shell.

# License

See [LICENSE](LICENSE.md).

# Build

```shell
make all
```

# Run

```shell
make run
```

# Play

Make changes to the [Dockerfile](Dockerfile) then build and run to see the impact on the image and the container.

```shell
# change Dockerfile
make all
docker ls
docker inspect process-inspector:0.1.0

make run
```
