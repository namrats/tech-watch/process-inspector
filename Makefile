DEFAULT_GOAL:=all

.PHONY: all clean docker run

DOCKER=docker
CARGO=cargo

PROJECT:=$(shell cargo read-manifest | jq -r '.name')
VERSION:=$(shell cargo read-manifest | jq -r '.version')
SRCS:=$(shell find src -name "*.rs")

TARGET:=target/x86_64-unknown-linux-musl/release/$(PROJECT)
IMAGE:=$(PROJECT)
TAG:=$(VERSION)

all: $(TARGET) docker

$(TARGET): $(SRCS) Cargo.toml
	$(CARGO) build --release

docker: $(TARGET)
	$(DOCKER) build --no-cache -t $(IMAGE):$(TAG) .

run:
	$(DOCKER) run --rm -it $(IMAGE):$(TAG)

clean:
	-$(DOCKER) image rm $(IMAGE):$(TAG)
	-$(CARGO) clean
