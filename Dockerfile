FROM scratch

WORKDIR /

COPY target/x86_64-unknown-linux-musl/release/process-inspector .

ENTRYPOINT ["/process-inspector"]
