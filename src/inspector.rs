use std::env;
use std::fs;
use std::io;
use std::process;
use std::path::Path;

use crate::error::{Result};

pub struct Inspector();

impl Inspector {
	pub fn new() -> Self {
		Inspector()
	}

	pub fn env(&self) -> Result<()> {
		println!("### Environment");
		for v in env::vars() {
			println!("{} = {}", v.0, v.1);
		}
		Ok(())
	}

	pub fn pid(&self) -> Result<()> {
	    println!("### PID: {}", process::id());
	    Ok(())
	}

	pub fn pwd(&self) -> Result<()> {
	    let path = env::current_dir()?;
	    println!("### PWD: {}", path.display());
	    Ok(())
	}

	pub fn args(&self) -> Result<()> {
		let args: Vec<String> = env::args().into_iter().collect();
		println!("### Arguments: {}", args.join(" "));
		Ok(())
	}

	pub fn filesystem(&self) -> Result<()> {
		println!("### Mounts");
		print_file(Path::new("/proc/self/mounts"))?;

	    println!("### FS");
	    visit_tree(Path::new("/"), "")?;
		Ok(())
	}

}

fn print_file(p: &Path) -> Result<()> {
	let mut file = fs::File::open(p)?;

	io::copy(&mut file, &mut io::stdout())?;

	Ok(())
}

fn visit_tree(p: &Path, prefix: &str) -> Result<()> {
    if p.is_dir() {
        for entry in fs::read_dir(p)? {
            let entry = entry?;
            let path = entry.path();
            println!("{}{}",
                prefix,
                path.file_name().unwrap_or_else(|| path.as_os_str()).to_str().unwrap());

			let file_type = path.symlink_metadata()?.file_type();
			if path.starts_with("/dev")
			|| path.starts_with("/proc")
			|| path.starts_with("/sys") {
				println!("{}  ...", prefix);
			} else if file_type.is_dir() && !file_type.is_symlink() {
				visit_tree(&path, &format!("{}  ", prefix))?;
            }
        }
    }
    Ok(())
}
