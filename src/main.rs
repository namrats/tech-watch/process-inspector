pub mod inspector;
pub mod error;

use self::inspector::Inspector;

fn main() -> error::Result<()> {
    println!("🕵️‍  Inspector 🕵️‍ ");

	let inspector = Inspector::new();

    inspector.pid()?;
    inspector.pwd()?;

	inspector.args()?;

	inspector.env()?;

    inspector.filesystem()?;

    Ok(())
}
